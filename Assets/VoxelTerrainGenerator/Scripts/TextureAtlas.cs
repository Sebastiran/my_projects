﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class TextureAtlas
    {
        public static readonly TextureAtlas _Instance = new TextureAtlas();
        public static Texture2D _Atlas { get; private set; }

        public void CreateAtlas()
        {
            string[] _Images = Directory.GetFiles("Assets/VoxelTerrainGenerator/Textures/", "*.png");
            foreach (string s in _Images)
            {
                Debug.Log("Image found for atlas: " + s);
            }

            int pixelWidth = 16;
            int pixelHeight = 16;
            int atlasWidth = (Mathf.CeilToInt(Mathf.Sqrt(_Images.Length)) * pixelWidth);
            int atlasHeight = (Mathf.CeilToInt(Mathf.Sqrt(_Images.Length)) * pixelHeight);
            Texture2D atlas = new Texture2D(atlasWidth, atlasHeight);

            int count = 0;
            for (int x = 0; x < atlasWidth / pixelWidth; x++)
            {
                for (int y = 0; y < atlasHeight / pixelHeight; y++)
                {
                    if (count > _Images.Length - 1)
                        goto end;

                    Texture2D temp = new Texture2D(0, 0);
                    temp.LoadImage(File.ReadAllBytes(_Images[count]));
                    
                    atlas.SetPixels(x * pixelWidth, y * pixelHeight, pixelWidth, pixelHeight, temp.GetPixels());

                    float startX = x * pixelWidth;
                    float startY = y * pixelHeight;
                    float perPixelRatioX = 1.0f / (float)atlas.width;
                    float perPixelRatioY= 1.0f / (float)atlas.height;
                    startX *= perPixelRatioX;
                    startY *= perPixelRatioY;
                    float endX = startX + (perPixelRatioX * pixelWidth);
                    float endY = startY + (perPixelRatioY * pixelHeight);

                    UVMap m = new UVMap(_Images[count], new Vector2[] {
                        new Vector2(startX, startY),
                        new Vector2(startX, endY),
                        new Vector2(endX, startY),
                        new Vector2(endX, endY),
                    });

                    m.Register();

                    count++;
                }
            }

        end:;
            _Atlas = atlas;
            File.WriteAllBytes("atlas.png", atlas.EncodeToPNG());
        }
    }
}