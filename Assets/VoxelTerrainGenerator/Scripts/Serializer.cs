﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace VoxelTerrainGenerator
{
    public class Serializer
    {
        public static bool CheckGenFolder(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(path);
                    return true;
                }
                catch (System.Exception e)
                {
                    Logger.Log(e.StackTrace.ToString());
                }
            }
            return false;
        }

        public static void SerializeToFile<T>(string path, string fileName, string extension, T DATA) where T : class
        {
            if (CheckGenFolder(path))
            {
                using (Stream s = File.OpenWrite(string.Format("{0}{1}.{2}", path, fileName, extension)))
                {
                    try
                    {
                        BinaryFormatter f = new BinaryFormatter();
                        f.Serialize(s, DATA);
                    }
                    catch (System.Exception e)
                    {
                        Logger.Log(e.StackTrace.ToString());
                    }
                }
            }
            else
            {
                throw new System.Exception("Can't get correct directory to save file");
            }
        }

        public static void SerializeToFileFullPath<T>(string path, T DATA) where T : class
        {
            try
            {
                using (Stream s = File.OpenWrite(path))
                {
                    BinaryFormatter f = new BinaryFormatter();
                    f.Serialize(s, DATA);
                }
            }
            catch (System.Exception e)
            {
                Logger.Log(e.StackTrace.ToString());
            }
        }

        public static T DeseralizeFromFile<T>(string path) where T : class
        {
            if (File.Exists(path))
            {
                using (Stream s = File.OpenRead(path))
                {
                    try
                    {
                        BinaryFormatter f = new BinaryFormatter();
                        return f.Deserialize(s) as T;
                    }
                    catch (System.Exception e)
                    {
                        Logger.Log(e.StackTrace.ToString() + "Error in deserialization");
                    }
                }
            }
            else
            {
                throw new System.Exception("File can not be found");
            }
            return null;
        }
    }
}