﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public interface ITickable
    {
        void Start();
        void Tick();
        void Update();
        void OnUnityUpdate();
    }
}