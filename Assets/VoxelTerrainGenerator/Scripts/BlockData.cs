﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    [System.Serializable]
    public class BlockData
    {
        public int ID;
        public object[] data;
    }
}