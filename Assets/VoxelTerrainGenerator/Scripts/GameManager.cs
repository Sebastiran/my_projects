﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

namespace VoxelTerrainGenerator
{
    public class GameManager : MonoBehaviour
    {
        private List<Delegate> delegates = new List<Delegate>();

        public Camera mCamera;
        public TMP_Text UIText;
        public Transform player;
        public Vector3 playerPos;

        public float dx = 50f;
        public float dz = 50f;
        public float my = 0.23f;
        public float cutoff = 1.8f;
        public float mul = 1f;

        public static float sDx = 50f;
        public static float sDz = 50f;
        public static float sMy = 0.23f;
        public static float sCutoff = 1.8f;
        public static float sMul = 1f;

        public static GameManager instance;
        private bool playerLoaded = false;

        private MainLoopable main;

        public void RegisterDelegate(Delegate d)
        {
            delegates.Add(d);
        }

        public void StartPlayer(Vector3 position)
        {
            GameObject.Destroy(mCamera.gameObject);
            GameObject.Destroy(UIText.gameObject);

            GameObject t = Instantiate(Resources.Load("VoxelTerrainGenerator/Player"), position, Quaternion.identity) as GameObject;
            player = t.transform;
        }

        void Start()
        {
            // Writing example
            //Serializer.SerializeToFile<object[]>("Data/Saves/World/Chunk/", "Chunk1", "chk", new object[] { "Bobby", "Adam", "Yui" });

            // Reading example
            //object[] o = Serializer.DeseralizeFromFile<object[]>("Data/Saves/World/Chunk/Chunk1.chk");
            //foreach (object i in o)
            //{
            //    Debug.Log("value: " + (string)i);
            //}
            FileManager.RegisterFiles();

            instance = this;
            TextureAtlas._Instance.CreateAtlas();

            MainLoopable.Instanciate();
            main = MainLoopable.GetInstance();
            main.Start();
        }

        void Update()
        {
            if (player != null)
            {
                playerPos = player.position;
                playerLoaded = true;
            }
            /*sDx = dx;
            sDz = dz;
            sMy = my;
            sCutoff = cutoff;
            sMul = mul;*/

            main.Update();

            foreach (Delegate d in new List<Delegate>(delegates))
            {
                d.DynamicInvoke();
                delegates.Remove(d);
            }
        }

        void OnApplicationQuit()
        {
            main.OnApplicationQuit();
        }

        internal static bool PlayerLoaded()
        {
            return GameManager.instance.playerLoaded;
        }
    }
}