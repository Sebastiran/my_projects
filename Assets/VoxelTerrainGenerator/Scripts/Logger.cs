﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class Logger : ILoopable
    {
        public static Logger MainLog = new Logger();
        private List<String> mainlogtxt = new List<string>();

        public static void Instanciate()
        {
            MainLoopable.GetInstance().RegisterLoops(MainLog);
        }

        public static void Log(string ll)
        {
            MainLog.log(ll);
        }
        public static void Log(System.Exception e)
        {
            MainLog.log(e);
        }

        private void log(string ll)
        {
            mainlogtxt.Add(ll);
        }
        private void log(System.Exception e)
        {
            mainlogtxt.Add(e.StackTrace.ToString());
        }

        public void Start()
        {
            //
        }

        public void Update()
        {
            System.IO.File.WriteAllLines("log.txt", new List<string>(mainlogtxt).ToArray());
        }

        public void OnApplicationQuit()
        {
            //
        }
    }
}