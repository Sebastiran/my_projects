﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

namespace VoxelTerrainGenerator
{
    public class World : ILoopable
    {
        public static World _instance { get; private set; }
        private Thread worldthread;
        private bool isRunning;
        
        private static Int3 playerPosition;
        private static readonly int renderDistance = 3;
        public static void Instanciate()
        {
            _instance = new World();
            MainLoopable.GetInstance().RegisterLoops(_instance);
            System.Random r = new System.Random();
            playerPosition = new Int3(r.Next(-1000, 1000), 100, r.Next(-1000, 1000));
        }

        public bool ranOnce = false;
        private List<Chunk> loadedChunks = new List<Chunk>();
        
        private Chunk firstChunk;
        public void Start()
        {
            isRunning = true;
            worldthread = new Thread(() =>
            {
                Logger.Log("Initialzing world thread");
                while (isRunning)
                {
                    try
                    {
                        if (!ranOnce)
                        {
                            for (int x = -renderDistance; x < renderDistance; x++)
                            {
                                for (int z = -renderDistance; z < renderDistance; z++)
                                {
                                    Int3 newChunkPos = new Int3(playerPosition.x, playerPosition.y, playerPosition.z);
                                    newChunkPos.AddPos(new Int3(x * Chunk.ChunkWidth, 0, z * Chunk.ChunkWidth));
                                    newChunkPos.ToChunkCoordinates();

                                    if (System.IO.File.Exists(FileManager.GetChunkString(newChunkPos.x, newChunkPos.z)))
                                    {
                                        loadedChunks.Add(new Chunk(newChunkPos.x, newChunkPos.z, this, Serializer.DeseralizeFromFile<int[,,]>(FileManager.GetChunkString(newChunkPos.x, newChunkPos.z))));
                                    }
                                    else
                                    {
                                        loadedChunks.Add(new Chunk(newChunkPos.x, newChunkPos.z, this));
                                    }
                                }
                            }

                            foreach (Chunk c in new List<Chunk>(loadedChunks))
                            {
                                c.Start();
                            }

                            ranOnce = true;
                        }

                        if (GameManager.PlayerLoaded())
                        {
                            playerPosition = new Int3(GameManager.instance.playerPos);
                        }
                        foreach (Chunk c in new List<Chunk>(loadedChunks))
                        {
                            if (Vector2.Distance(new Vector2(c.PosX * Chunk.ChunkWidth, c.PosZ * Chunk.ChunkWidth), new Vector2(playerPosition.x, playerPosition.z)) > ((renderDistance + 1) * Chunk.ChunkWidth))
                            {
                                c.Degenerated();
                            }
                        }
                        for (int x = -renderDistance; x < renderDistance; x++)
                        {
                            for (int z = -renderDistance; z < renderDistance; z++)
                            {
                                Int3 newChunkPos = new Int3(playerPosition.x, playerPosition.y, playerPosition.z);
                                newChunkPos.AddPos(new Int3(x * Chunk.ChunkWidth, 0, z * Chunk.ChunkWidth));
                                newChunkPos.ToChunkCoordinates();
                                if (!ChunkExists(newChunkPos.x, newChunkPos.z))
                                {
                                    if (System.IO.File.Exists(FileManager.GetChunkString(newChunkPos.x, newChunkPos.z)))
                                    {
                                        Chunk c = new Chunk(newChunkPos.x, newChunkPos.z, this, Serializer.DeseralizeFromFile<int[,,]>(FileManager.GetChunkString(newChunkPos.x, newChunkPos.z)));
                                        c.Start();
                                        loadedChunks.Add(c);
                                    }
                                    else
                                    {
                                        Chunk c = new Chunk(newChunkPos.x, newChunkPos.z, this);
                                        c.Start();
                                        loadedChunks.Add(c);
                                    }
                                }
                            }
                        }

                        foreach (Chunk c in new List<Chunk>(loadedChunks))
                        {
                            c.Update();
                        }
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(e.StackTrace);
                        Logger.Log(e);
                    }
                }
                Logger.Log("World thread succesfully stopped");
                Logger.MainLog.Update(); // Rerun last log;

            });
            worldthread.Start();
        }

        internal void RemoveChunk(Chunk chunk)
        {
            loadedChunks.Remove(chunk);
        }

        public bool ChunkExists(int posX, int posZ)
        {
            foreach (Chunk c in new List<Chunk>(loadedChunks))
            {
                if (c.PosX.Equals(posX) && c.PosZ.Equals(posZ))
                {
                    return true;
                }
            }
            return false;
        }

        public Chunk GetChunk(int posX, int posZ)
        {
            foreach (Chunk c in new List<Chunk>(loadedChunks))
            {
                if (c.PosX.Equals(posX) && c.PosZ.Equals(posZ))
                {
                    return c;
                }
            }

            return new ErroredChunk(posX, posZ, this);
        }

        public void Update()
        {
            foreach (Chunk c in new List<Chunk>(loadedChunks))
            {
                if (c != null)
                {
                    c.OnUnityUpdate();
                }
            }
        }

        public void OnApplicationQuit()
        {
            foreach (Chunk c in new List<Chunk>(loadedChunks))
            {
                Serializer.SerializeToFileFullPath<int[,,]>(FileManager.GetChunkString(c.PosX, c.PosZ), c.GetChunkSaveData());
            }
            isRunning = false;
            Logger.Log("Stopping world thread");
        }
    }
}