﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class Block : ITickable
    {
        private bool isTransparent;

        public static Block dirt = new Block("Dirt", false, "Assets/VoxelTerrainGenerator/Textures/dirt.png");
        public static Block sand = new Block("Sand", false, "Assets/VoxelTerrainGenerator/Textures/sand.png");
        public static Block air = new Block("Air", true);
        private string name;
        private Vector2[] _UVMap;
        private static int _currentID = 0;
        private int ID;
        private string blockName;

        public Block(string blockName, bool isTransparent)
        {
            this.blockName = blockName;
            this.isTransparent = isTransparent;
            Register();
        }

        public Block(string blockName, bool isTransparent, string name)
        {
            this.blockName = blockName;
            this.isTransparent = isTransparent;
            this.name = name;

            _UVMap = UVMap.GetUVMap(name)._UVMAP;
            Register();
        }

        private void Register()
        {
            ID = _currentID;
            _currentID++;
            BlockRegistry.RegisterBlock(this);
        }

        public string GetBlockName()
        {
            return blockName;
        }

        public int GetID()
        {
            return ID;
        }

        public bool IsTransparent()
        {
            return isTransparent;
        }

        public void Start()
        {
            
        }

        public void Tick()
        {
            
        }

        public void Update()
        {
            
        }

        public void OnUnityUpdate()
        {

        }

        public virtual MeshData Draw(Chunk chunk, Block[,,] _Blocks, int x, int y, int z)
        {
            if (this.Equals(air))
            {
                return new MeshData();
            }
            return MathHelper.DrawCube(chunk, _Blocks, this, x, y, z, this._UVMap);
        }
    }
}