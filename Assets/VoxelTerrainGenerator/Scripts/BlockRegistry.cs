﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class BlockRegistry
    {
        private static readonly bool debugMode = false;
        private static List<Block> _registeredBlocks = new List<Block>();

        public static void RegisterBlock(Block b)
        {
            _registeredBlocks.Add(b);
        }

        public static void RegisterBlocks()
        {
            if (debugMode)
            {
                int i = 0;
                List<string> names = new List<string>();
                foreach (Block b in _registeredBlocks)
                {
                    names.Add(string.Format("CurrentID: {0}, Blockname: {1}, BlockID: {2}", i, b.GetBlockName(), b.GetID()));
                    i++;
                }
                System.IO.File.WriteAllLines("BlockRegistry.txt", names.ToArray());
            }
        }

        public static Block GetBlockFromID(int v)
        {
            try
            {
                return _registeredBlocks[v];
            }
            catch (System.Exception e)
            {
                Debug.Log(e.ToString());
            }
            return null;
        }
    }
}