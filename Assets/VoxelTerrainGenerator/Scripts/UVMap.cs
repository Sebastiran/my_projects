﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class UVMap
    {
        private static List<UVMap> _Maps = new List<UVMap>();

        public string name;
        public Vector2[] _UVMAP;

        public UVMap(string name, Vector2[] _UVMAP)
        {
            this.name = name;
            this._UVMAP = _UVMAP;
        }

        public void Register()
        {
            _Maps.Add(this);
        }

        public static UVMap GetUVMap(string name)
        {
            foreach (UVMap m in _Maps)
            {
                if (m.name.Equals(name))
                {
                    return m;
                }
            }
            
            return _Maps[0];
        }
    }
}