﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class Chunk : ITickable
    {
        private static bool firstChunk = false;
        private bool isFirstChunk = false;
        private World world;

        public static readonly int ChunkWidth = 20;
        public static readonly int ChunkHeight = 20;

        private Block[,,] _Blocks;

        public int PosX { get; private set; }
        public int PosZ { get; private set; }
        public Chunk(int px, int pz, World world)
        {
            PosX = px;
            PosZ = pz;
            this.world = world;
        }
        public Chunk(int px, int pz, World world, int[,,] _data)
        {
            PosX = px;
            PosZ = pz;
            LoadChunkFromData(_data);
            hasGenerated = true;
            this.world = world;
        }

        public float GetHeight(float px, float py, float pz)
        {
            px += PosX * ChunkWidth;
            pz += PosZ * ChunkWidth;

            float p1 = Mathf.PerlinNoise(px / GameManager.sDx, pz / GameManager.sDz) * GameManager.sMul;
            p1 *= GameManager.sMy * py;

            return p1;
        }

        protected bool hasGenerated = false;
        public virtual void Start()
        {
            if (!firstChunk)
            {
                firstChunk = true;
                isFirstChunk = true;
            }

            if (hasGenerated)
            {
                return;
            }

            hasGenerated = true;
            _Blocks = new Block[ChunkWidth, ChunkHeight, ChunkWidth];
            for (int x = 0; x < ChunkWidth; x++)
            {
                for (int y = 0; y < ChunkHeight; y++)
                {
                    for (int z = 0; z < ChunkWidth; z++)
                    {
                        float perlin = GetHeight(x, y, z);
                        if (perlin > GameManager.sCutoff)
                        {
                            _Blocks[x, y, z] = Block.air;
                        }
                        else
                        {
                            if (perlin > GameManager.sCutoff/2f)
                            {
                                _Blocks[x, y, z] = Block.dirt;
                            }
                            else
                            {
                                _Blocks[x, y, z] = Block.sand;
                            }
                        }
                    }
                }
            }
        }
        public void Tick()
        {

        }

        public void Degenerated()
        {
            try
            {
                Serializer.SerializeToFileFullPath<int[,,]>(FileManager.GetChunkString(PosX, PosZ), GetChunkSaveData());
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }

            GameManager.instance.RegisterDelegate(new Action(() =>
            {
                GameObject.Destroy(go);
            }));
            world.RemoveChunk(this);
        }
        public int[,,] GetChunkSaveData()
        {
            return _Blocks.ToIntArray();
        }
        public void LoadChunkFromData(int [,,] data)
        {
            _Blocks = data.ToBlockArray();
        }

        protected bool hasDrawn = false;
        protected bool drawLock = false;
        private bool needsToUpdate = false;
        MeshData data;
        public virtual void Update()
        {
            if (needsToUpdate)
            {
                if (!drawLock && !renderLock)
                {
                    hasDrawn = false;
                    hasRendered = false;
                    needsToUpdate = false;
                }
            }

            if (!hasDrawn && hasGenerated && !drawLock)
            {
                drawLock = true;

                data = new MeshData();
                for (int x = 0; x < ChunkWidth; x++)
                {
                    for (int y = 0; y < ChunkHeight; y++)
                    {
                        for (int z = 0; z < ChunkWidth; z++)
                        {
                            data.Merge(_Blocks[x,y,z].Draw(this, _Blocks, x, y, z));
                        }
                    }
                }
                hasDrawn = true;
                drawLock = false;
            }
        }

        protected bool hasRendered = false;
        private bool renderLock = false;
        protected GameObject go;
        public virtual void OnUnityUpdate()
        {
            if (hasGenerated && !hasRendered && hasDrawn && !renderLock)
            {
                renderLock = true; 

                hasRendered = true;
                Mesh mesh = data.ToMesh();
                if (go == null)
                {
                    go = new GameObject();
                }

                Transform t = go.transform;
                if (t.gameObject.GetComponent<MeshFilter>() == null)
                {
                    t.gameObject.AddComponent<MeshFilter>();
                    t.gameObject.AddComponent<MeshRenderer>();
                    t.gameObject.AddComponent<MeshCollider>();
                    //t.gameObject.GetComponent<MeshRenderer>().material = Resources.Load<Material>("VoxelTerrainGenerator/Dirt");
                    t.position = new Vector3(PosX * ChunkWidth, 0, PosZ * ChunkWidth);
                    Texture2D tmp = new Texture2D(0, 0);
                    tmp.LoadImage(System.IO.File.ReadAllBytes("atlas.png"));
                    tmp.filterMode = FilterMode.Point;
                    t.gameObject.GetComponent<MeshRenderer>().material.mainTexture = tmp;
                }

                t.transform.GetComponent<MeshFilter>().sharedMesh = mesh;
                t.transform.GetComponent<MeshCollider>().sharedMesh = mesh;

                renderLock = false;

                if (isFirstChunk)
                {
                    GameManager.instance.StartPlayer(new Vector3(PosX * ChunkWidth, 100, PosZ * ChunkWidth));
                }
            }
        }

        internal void SetBlock(int x, int y, int z, Block block)
        {
            _Blocks[x, y, z] = block;
            needsToUpdate = true;
        }
    }
}