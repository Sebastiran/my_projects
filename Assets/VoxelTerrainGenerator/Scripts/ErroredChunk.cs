﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class ErroredChunk : Chunk
    {
        public ErroredChunk(int px, int py, World world) : base(px, py, world)
        {

        }

        public override void Start()
        {
            throw new Exception("Tried to use start in erroredchunk class");
        }

        public override void Update()
        {
            throw new Exception("Tried to use update in erroredchunk class");
        }

        public override void OnUnityUpdate()
        {
            throw new Exception("Tried to use onunityupdate in erroredchunk class");
        }
    }
}
