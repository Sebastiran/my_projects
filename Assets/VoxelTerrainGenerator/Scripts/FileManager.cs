﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class FileManager
    {
        public static readonly string chunkSaveDirectory = "Data/World/DevWorld/Chuncks/";

        public static void RegisterFiles()
        {
            Serializer.CheckGenFolder(chunkSaveDirectory);
        }
        
        public static string GetChunkString(int x, int z)
        {
            return string.Format("{0}C{1}_{2}.CHK", chunkSaveDirectory, x, z);
        }
    }
}