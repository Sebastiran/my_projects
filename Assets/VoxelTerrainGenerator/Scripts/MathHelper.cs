﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class Int3
    {
        public int x, y, z;
        public Int3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Int3(Vector3 position)
        {
            this.x = (int)position.x;
            this.y = (int)position.y;
            this.z = (int)position.z;
        }

        public void AddPos(Int3 int3)
        {
            this.x += int3.x;
            this.y += int3.y;
            this.z += int3.z;
        }

        public override string ToString()
        {
            return string.Format("X:{0}, Y:{1}, Z:{2}", x, y, z);
        }

        internal void ToChunkCoordinates()
        {
            this.x = Mathf.FloorToInt(x / Chunk.ChunkWidth);
            this.z = Mathf.FloorToInt(z / Chunk.ChunkWidth);
        }
    }

    public class MathHelper
    {
        public static MeshData DrawCube(Chunk chunk, Block[,,] _Blocks, Block block, int x, int y, int z, Vector2[] uvmap)
        {
            MeshData d = new MeshData();

            if (block.Equals(Block.air))
            {
                return new MeshData();
            }

            // Bottom face
            if (y - 1 < 0 || _Blocks[x, y - 1, z].IsTransparent())
            {
                d.Merge(new MeshData(
                    new List<Vector3>()
                    {
                        new Vector3(0,0,1),
                        new Vector3(1,0,1),
                        new Vector3(0,0,0),
                        new Vector3(1,0,0)
                    },
                    new List<int>()
                    {
                        0, 2, 3, 0, 3, 1
                    },
                    uvmap
                    ));
            }

            // Top face
            if (y + 1 >= Chunk.ChunkHeight || _Blocks[x, y + 1, z].IsTransparent())
            {
                d.Merge(new MeshData(
                    new List<Vector3>()
                    {
                        new Vector3(0,1,0),
                        new Vector3(1,1,0),
                        new Vector3(0,1,1),
                        new Vector3(1,1,1)
                    },
                    new List<int>()
                    {
                        0, 2, 3, 0, 3, 1
                    },
                    uvmap
                    ));
            }

            // Right face
            if (x + 1 >= Chunk.ChunkWidth || _Blocks[x + 1, y, z].IsTransparent())
            {
                d.Merge(new MeshData(
                    new List<Vector3>()
                    {
                        new Vector3(1,1,1),
                        new Vector3(1,1,0),
                        new Vector3(1,0,1),
                        new Vector3(1,0,0)
                    },
                    new List<int>()
                    {
                        0, 2, 3, 0, 3, 1
                    },
                    uvmap
                    ));
            }

            // Left face
            if (x - 1 < 0 || _Blocks[x - 1, y, z].IsTransparent())
            {
                d.Merge(new MeshData(
                    new List<Vector3>()
                    {
                        new Vector3(0,1,0),
                        new Vector3(0,1,1),
                        new Vector3(0,0,0),
                        new Vector3(0,0,1)
                    },
                    new List<int>()
                    {
                        0, 2, 3, 0, 3, 1
                    },
                    uvmap
                    ));
            }

            // Front face
            if (z + 1 >= Chunk.ChunkWidth || _Blocks[x, y, z + 1].IsTransparent())
            {
                d.Merge(new MeshData(
                    new List<Vector3>()
                    {
                        new Vector3(0,1,1),
                        new Vector3(1,1,1),
                        new Vector3(0,0,1),
                        new Vector3(1,0,1)
                    },
                    new List<int>()
                    {
                        0, 2, 3, 0, 3, 1
                    },
                    uvmap
                    ));
            }

            // Back face
            if (z - 1 < 0 || _Blocks[x, y, z - 1].IsTransparent())
            {
                d.Merge(new MeshData(
                    new List<Vector3>()
                    {
                        new Vector3(1,1,0),
                        new Vector3(0,1,0),
                        new Vector3(1,0,0),
                        new Vector3(0,0,0)
                    },
                    new List<int>()
                    {
                        0, 2, 3, 0, 3, 1
                    },
                    uvmap
                    ));
            }

            d.AddPos(new Vector3(x, y, z));

            return d;
        }

        internal static void AddBlock(Vector3 roundedPosition, Block block)
        {
            if (roundedPosition.y >= Chunk.ChunkHeight)
            {
                return;
            }

            int chunkPosX = Mathf.FloorToInt(roundedPosition.x / Chunk.ChunkWidth);
            int chunkPosZ = Mathf.FloorToInt(roundedPosition.z / Chunk.ChunkWidth);
            Chunk currentChunk;

            try
            {
                currentChunk = World._instance.GetChunk(chunkPosX, chunkPosZ);
                if (currentChunk.GetType().Equals(typeof(ErroredChunk)))
                {
                    Debug.Log("Current chunk is errored: " + roundedPosition.ToString());
                    return;
                }

                int x = (int)(roundedPosition.x - (chunkPosX * Chunk.ChunkWidth));
                int z = (int)(roundedPosition.z - (chunkPosZ * Chunk.ChunkWidth));
                int y = (int)roundedPosition.y;
                currentChunk.SetBlock(x, y, z, block);
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }
}