﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class CharacterController : MonoBehaviour
    {
        Transform add;
        Transform delete;

        void Start()
        {
            add = Instantiate(Resources.Load<Transform>("VoxelTerrainGenerator/Add").transform);
            delete = Instantiate(Resources.Load<Transform>("VoxelTerrainGenerator/Delete").transform);
        }

        void Update()
        {
            if (Input.GetMouseButton(0))
            {
                add.transform.GetComponent<MeshRenderer>().enabled = true;

                Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Vector3 rawPosition = hit.point + (hit.normal / 2);
                    Vector3 roundedPosition = new Vector3(Mathf.FloorToInt(rawPosition.x), Mathf.FloorToInt(rawPosition.y), Mathf.FloorToInt(rawPosition.z));
                    add.transform.position = roundedPosition + (Vector3.one / 2);
                    Debug.Log(roundedPosition);
                    MathHelper.AddBlock(roundedPosition, Block.sand);
                }
            }
            else
            {
                if (add.transform.GetComponent<MeshRenderer>().enabled == true)
                {
                    add.transform.GetComponent<MeshRenderer>().enabled = false;
                }
            }

            if (Input.GetMouseButton(1))
            {
                delete.transform.GetComponent<MeshRenderer>().enabled = true;

                Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Vector3 rawPosition = hit.point - (hit.normal / 2);
                    Vector3 roundedPosition = new Vector3(Mathf.FloorToInt(rawPosition.x), Mathf.FloorToInt(rawPosition.y), Mathf.FloorToInt(rawPosition.z));
                    delete.transform.position = roundedPosition + (Vector3.one / 2);
                    MathHelper.AddBlock(roundedPosition, Block.air);
                }
            }
            else
            {
                if (delete.transform.GetComponent<MeshRenderer>().enabled == true)
                {
                    delete.transform.GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }
    }
}