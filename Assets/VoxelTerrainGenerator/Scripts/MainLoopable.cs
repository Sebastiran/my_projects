﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public class MainLoopable : ILoopable
    {
        private static MainLoopable _Instance;
        private List<ILoopable> _RegisteredLoops = new List<ILoopable>();

        public static void Instanciate()
        {
            _Instance = new MainLoopable();

            //register
            Logger.Instanciate();
            World.Instanciate();
            Block.air.GetBlockName();   //Done to make sure the blocks register before calling the BlockRegistry
            BlockRegistry.RegisterBlocks();
        }

        public void RegisterLoops(ILoopable l)
        {
            _RegisteredLoops.Add(l);
        }

        public void DeregisterLoops(ILoopable l)
        {
            _RegisteredLoops.Remove(l);
        }

        public static MainLoopable GetInstance()
        {
            return _Instance;
        }

        public void Start()
        {
            foreach (ILoopable l in _RegisteredLoops)
            {
                l.Start();
            }
        }

        public void Update()
        {   
            foreach (ILoopable l in _RegisteredLoops)
            {
                l.Update();
            }
        }

        public void OnApplicationQuit()
        {
            foreach (ILoopable l in _RegisteredLoops)
            {
                l.OnApplicationQuit();
            }
        }
    }
}