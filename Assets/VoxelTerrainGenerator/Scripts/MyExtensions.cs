﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public static class MyExtensions
    {
        public static int[,,] ToIntArray(this Block[,,] chunkData)
        {
            int lx = chunkData.GetLength(0);
            int ly = chunkData.GetLength(1);
            int lz = chunkData.GetLength(2);

            int[,,] data = new int[lx, ly, lz];
            for (int x = 0; x < lx; x++)
            {
                for (int y = 0; y < ly; y++)
                {
                    for (int z = 0; z < lz; z++)
                    {
                        data[x, y, z] = chunkData[x, y, z].GetID();
                    }
                }
            }
            return data;    
        }

        public static Block[,,] ToBlockArray(this int[,,] _data)
        {
            int lx = _data.GetLength(0);
            int ly = _data.GetLength(1);
            int lz = _data.GetLength(2);

            Block[,,] chunkData = new Block[lx, ly, lz];
            for (int x = 0; x < lx; x++)
            {
                for (int y = 0; y < ly; y++)
                {
                    for (int z = 0; z < lz; z++)
                    {
                        chunkData[x, y, z] = BlockRegistry.GetBlockFromID(_data[x, y, z]);
                    }
                }
            }
            return chunkData;
        }
    }
}