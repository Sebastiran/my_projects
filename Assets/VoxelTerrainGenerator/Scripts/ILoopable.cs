﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VoxelTerrainGenerator
{
    public interface ILoopable
    {
        void Start();
        void Update();
        void OnApplicationQuit();
    }
}